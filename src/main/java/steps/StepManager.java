package steps;

public class StepManager {
    public String baseUrl;
    public SearchSteps searchSteps;
    public UserSteps userSteps;
    public CartSteps cartSteps;

    public StepManager(String baseUrl) {
        this.baseUrl = baseUrl;
        initAllSteps();
    }

    private void initAllSteps(){
        searchSteps = new SearchSteps(baseUrl);
        userSteps = new UserSteps(baseUrl);
        cartSteps = new CartSteps(baseUrl);
    }
}
