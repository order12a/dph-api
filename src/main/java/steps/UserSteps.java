package steps;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import models.User;
import models.enums.Apikey;
import ru.yandex.qatools.allure.annotations.Step;

public class UserSteps {
    public String baseUrl;

    public UserSteps(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Step
    public HttpResponse<JsonNode> login(User user, Apikey apikey) throws UnirestException {
        return Unirest.get(baseUrl)
                .queryString("dp_command", "login")
                .queryString("dp_apikey", apikey.getValue())
                .queryString("dp_login_user", user.getLogin())
                .queryString("dp_login_password", user.getPassword())
                .asJson();
    }

    @Step
    public String getSessionId(User user, Apikey apikey) throws UnirestException {
        HttpResponse<JsonNode> response = Unirest.get(baseUrl)
                .queryString("dp_command", "login")
                .queryString("dp_apikey", apikey.getValue())
                .queryString("dp_login_user", user.getLogin())
                .queryString("dp_login_password", user.getPassword())
                .asJson();
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(response.getBody().toString());
        return JsonPath.read(document, "$..sessionid").toString().replaceAll("[^a-zA-Z0-9]", "");
    }

}
