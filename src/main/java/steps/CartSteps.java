package steps;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import models.enums.Apikey;
import ru.yandex.qatools.allure.annotations.Step;

public class CartSteps {
    public String baseUrl;

    public CartSteps(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Step
    public HttpResponse<JsonNode> clearCart(String sessionId, Apikey apikey) throws UnirestException {
        return Unirest.get(baseUrl)
                .queryString("dp_command", "clearCart")
                .queryString("dp_apikey", sessionId)
                .queryString("dp_session_id", apikey.getValue())
                .asJson();
    }
}
