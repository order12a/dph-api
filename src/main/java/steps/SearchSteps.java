package steps;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import models.enums.Apikey;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.*;

public class SearchSteps {

    public String baseUrl;

    public SearchSteps(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Step
    public HttpResponse<JsonNode> search(String query, Apikey apikey) throws UnirestException {
        return Unirest.get(baseUrl)
                .queryString("dp_command", "search")
                .queryString("dp_apikey", apikey.getValue())
                .queryString("dp_search_query", query)
                .asJson();
    }

    @Step
    public HttpResponse<JsonNode> search(Map<String, Object> params) throws UnirestException {
        return Unirest.get(baseUrl)
                .queryString(params)
                .asJson();
    }

    @Step
    public List<String> getSearchResults(Map<String, Object> params, Apikey apikey, int quantity) throws UnirestException {
        HttpResponse<JsonNode> response = Unirest.get(baseUrl)
                .queryString("dp_command", "search")
                .queryString("dp_apikey", apikey.getValue())
                .queryString("dp_search_limit", quantity)
                .queryString(params)
                .asJson();

        Object document = Configuration.defaultConfiguration().jsonProvider().parse(response.getBody().toString());
        String ids = JsonPath.read(document, "$..id").toString().replaceAll("\"", "");
        ids = ids.substring(1, ids.length() - 1);
        List<String> results = new ArrayList<String>(Arrays.asList(ids.split(",")));
        return results;
    }

    @Step
    public List<String> getSearchResultsDefault() throws UnirestException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("dp_search_query", "gun");
        return getSearchResults(map, Apikey.APPLICATIONS_API_KEY, 10);
    }
}
