package models.enums;

public enum UserType {
    BUYER,
    SELLER,
    ENTERPRISE
}
