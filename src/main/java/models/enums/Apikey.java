package models.enums;

public enum Apikey {
    PARTNER_API_KEY,
    APPLICATIONS_API_KEY,
    RESELLER_API_KEY;

    public String getValue(){
        switch (this){
            case PARTNER_API_KEY:
                return "f851c05136d4c64ce736289252c0470d04a1b870";
            case RESELLER_API_KEY:
                return "7748c335145f5cc8404f4b22801b4fe464e8312f";
            case APPLICATIONS_API_KEY:
            default:
                return "d3be9edccb0822bdc1ec30467e91855b93ee120c";
        }
    }

    @Override
    public String toString() {
        return "API Key " + this.getValue();
    }
}
