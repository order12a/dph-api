package models;

public class User {
    private String login;
    private String password;
    private String email;
    private String id;
    private String apiKey;
    private String firstName;
    private String lastName;
    private String country;
    private String city;
    private String zip;

    public User(){

    }

    //Default Constructor
    public User(String login, String password){
        this.login = login;
        this.password = password;
    }

    private User(Builder builder) {
        setLogin(builder.login);
        setPassword(builder.password);
        setEmail(builder.email);
        setId(builder.id);
        setApiKey(builder.apiKey);
        setFirstName(builder.firstName);
        setLastName(builder.lastName);
        setCountry(builder.country);
        setCity(builder.city);
        setZip(builder.zip);
    }

    @Override
    public String toString(){//Simple toString to improve allure reports(show params)
        StringBuilder builder = new StringBuilder();
        builder.append("login - " + this.login + " ").append("password - " + this.password + " ");
        return builder.toString();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public static Builder newBuilder(){
        return new User().new Builder();
    }

    public class Builder {
        private String login;
        private String password;
        private String email;
        private String id;
        private String apiKey;
        private String firstName;
        private String lastName;
        private String country;
        private String city;
        private String zip;

        public Builder() {
        }

        public Builder setLogin(String val) {
            login = val;
            return this;
        }

        public Builder setPassword(String val) {
            password = val;
            return this;
        }

        public Builder setEmail(String val) {
            email = val;
            return this;
        }

        public Builder setId(String val) {
            id = val;
            return this;
        }

        public Builder setApiKey(String val) {
            apiKey = val;
            return this;
        }

        public Builder setFirstName(String val) {
            firstName = val;
            return this;
        }

        public Builder setLastName(String val) {
            lastName = val;
            return this;
        }

        public Builder setCountry(String val) {
            country = val;
            return this;
        }

        public Builder setCity(String val) {
            city = val;
            return this;
        }

        public Builder setZip(String val) {
            zip = val;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }
}
