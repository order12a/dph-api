import com.mashape.unirest.http.exceptions.UnirestException;
import config.BaseTestData;
import models.User;
import models.enums.Apikey;
import org.testng.annotations.Test;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class CartTest extends BaseTestData{

    @Test(invocationCount = 4)
    public void addToCart() throws UnirestException {
        User user = User.newBuilder().setLogin("usefOne").setPassword("123456").setId("4133891").build();
        List<String> itemIds = app.searchSteps.getSearchResultsDefault();
        String sessionId = app.userSteps.getSessionId(user, Apikey.APPLICATIONS_API_KEY);
        app.cartSteps.clearCart(sessionId, Apikey.APPLICATIONS_API_KEY);

        given().param("dp_command", "addToCart")
                .param("dp_apikey", Apikey.APPLICATIONS_API_KEY.getValue())
                .param("dp_session_id", sessionId)
                .param("dp_media_id", itemIds.get(0))
                .when()
                .get(baseUrl)
                .then()
                .body("type", equalTo("success"));
    }
}
