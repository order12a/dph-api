import com.mashape.unirest.http.exceptions.UnirestException;
import config.BaseTestData;
import models.enums.Apikey;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class SearchTest extends BaseTestData{

    @Test(invocationCount = 4)
    public void simpleSearch(){
        given().param("dp_apikey", Apikey.APPLICATIONS_API_KEY.getValue())
                .param("dp_command", "search")
                .param("dp_search_query", "sun")
                .when()
                .get(baseUrl)
                .then()
                .statusCode(200)
                .body("count", greaterThan(100000))
                .body("result", not(empty()))
                .body("type", equalToIgnoringCase("success"));
    }

    @Test(invocationCount = 3)
    @Description("Invoking search tips from search engine")
    public void searchHint(){
        given().param("dp_apikey", Apikey.APPLICATIONS_API_KEY.getValue())
                .param("dp_command", "searchHint")
                .param("dp_search_prefix", "wom")
                .param("dp_language", "en")
                .when().get(baseUrl)
                .then()
                .statusCode(200)
                .body("hints", hasSize(greaterThan(3)))
                .body("hints[0]", containsString("woman"));
    }


    @Test(enabled = false)
    public void testSearchHelper() throws UnirestException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("dp_search_query", "gun");
        app.searchSteps.getSearchResults(map, Apikey.APPLICATIONS_API_KEY, 3);
    }
}
