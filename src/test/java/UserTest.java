import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import config.BaseTestData;
import models.User;
import models.enums.Apikey;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class UserTest extends BaseTestData{

    @Test(invocationCount = 2)
    public void partnerLogin() throws UnirestException {
        User user = User.newBuilder().setLogin("usefOne").setPassword("123456").setId("4133891").build();
        HttpResponse<JsonNode> response = app.userSteps.login(user, Apikey.PARTNER_API_KEY);
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(response.getBody().toString());

        Assert.assertEquals(response.getStatus(), 200);
        Assert.assertEquals(JsonPath.read(document, "$.userId"), user.getId());
        Assert.assertEquals(JsonPath.read(document, "$.apiKey"), Apikey.PARTNER_API_KEY.getValue());
        Assert.assertEquals(JsonPath.read(document, "$.type"), "success");
        Assert.assertTrue(JsonPath.read(document, "$.sessionid").toString().length() > 10);
    }

    @Test(invocationCount = 2)
    public void partnerLoginFailed() throws UnirestException {
        User user = User.newBuilder().setLogin("usefOne").setPassword("xxxxx").setId("34643543").build();
        HttpResponse<JsonNode> response = app.userSteps.login(user, Apikey.PARTNER_API_KEY);
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(response.getBody().toString());

        Assert.assertEquals(response.getStatus(), 200);
        Assert.assertEquals(JsonPath.read(document, "$.userId"), user.getId());
        Assert.assertEquals(JsonPath.read(document, "$.apiKey"), Apikey.PARTNER_API_KEY.getValue());
        Assert.assertEquals(JsonPath.read(document, "$.type"), "success");
        Assert.assertTrue(JsonPath.read(document, "$.sessionid").toString().length() > 10);
    }

    @Test(invocationCount = 2)
    public void userLoginRestAssured(){
        User user = User.newBuilder().setLogin("usefOne").setPassword("123456").setId("4133891").build();
        String document = given().param("dp_command", "login")
                .param("dp_apikey", Apikey.APPLICATIONS_API_KEY.getValue())
                .param("dp_login_user", user.getLogin())
                .param("dp_login_password", user.getPassword())
                .when().get(baseUrl)
                .then()
                .statusCode(200)
                .body("userId", equalTo(user.getId()))
                .body("apiKey", equalTo(Apikey.APPLICATIONS_API_KEY.getValue()))
                .body("type", equalTo("success"))
                .body("sessionid", not(empty()))
                .extract().response().asString();
        String sessionId = JsonPath.parse(document).read("$.sessionid").toString();
        Assert.assertTrue(sessionId.length() > 10);
    }

    @Test(invocationCount = 2)
    public void userLoginRestAssuredFailed(){
        User user = User.newBuilder().setLogin("usefOne").setPassword("xxxxxx").setId("4133891").build();
        String document = given().param("dp_command", "login")
                .param("dp_apikey", Apikey.APPLICATIONS_API_KEY.getValue())
                .param("dp_login_user", user.getLogin())
                .param("dp_login_password", user.getPassword())
                .when().get(baseUrl)
                .then()
                .statusCode(200)
                .body("userId", equalTo(user.getId()))
                .body("apiKey", equalTo(Apikey.APPLICATIONS_API_KEY.getValue()))
                .body("type", equalTo("success"))
                .body("sessionid", not(empty()))
                .extract().response().asString();
        String sessionId = JsonPath.parse(document).read("$.sessionid").toString();
        Assert.assertTrue(sessionId.length() > 10);
    }
}


