package config;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import steps.StepManager;

public class BaseTest {
    public static String baseUrl;
    public StepManager app;

    @BeforeClass
    public static void setUp(){
        baseUrl = "http://" + System.getProperty("baseUrl");
    }

    @BeforeMethod
    public void initSteps(){
        app = new StepManager(baseUrl);
    }
}
